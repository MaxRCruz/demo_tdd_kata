# coding=utf-8
from unittest import TestCase

from Calculadora import Calculadora


class CalculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEqual(Calculadora().sumar(""), 0, "Cadena vacía")

    def test_sumar_una_cadena(self):
        self.assertEqual(Calculadora().sumar("1"), 1, "Un número")

    def test_sumar_cadena_con_un_numero(self):
        self.assertEqual(Calculadora().sumar("1"), 1, "Un número")
        self.assertEqual(Calculadora().sumar("2"), 2, "Un número")

    def test_sumar_cadena_con_dos_numeros(self):
        self.assertEqual(Calculadora().sumar("1,3"), 4, "Dos números")

    def test_sumar_cadena_con_multiples_numeros(self):
        self.assertEqual(Calculadora().sumar("5,2,4,1"), 12, "Múltiples números")

    def test_sumar_cadena_con_multiples_numeros_multiples_separadores(self):
        self.assertEqual(Calculadora().sumar("5,2&4:1:2&8"), 22, "Múltiples números distintos separadores")
